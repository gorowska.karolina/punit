'''
Created on 5 Mar 2016

@author: PM
'''

class GaDeRyPoLuKi(object):
    '''
    classdocs
    '''

    def __init__(self):
        self.map = {"g":"a", "a":"g", "d":"e", "e":"d", "r":"y", "y":"r", "p":"o", "o":"p", "l":"u", "u":"l", "k":"i", "i":"k"};
        
        '''
        Constructor
        '''
        
    def translate(self, msg):
        result = ""
        for c in msg:
            if c in self.map.keys():
                result += self.map[c];
            else:
                result += c;
                
        return result;
    
    def translate_ignore_case(self, msg):
        return self.translate(msg.lower());
    
    def is_translatable(self, c):
        return c in self.map.keys();
    
    def get_code_lengh(self):
        return len(self.map.keys());