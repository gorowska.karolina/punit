import unittest
from pl.poznan.put.spio.stack import Stack

class StackTest(unittest.TestCase):
    
    stack = None;

    def setUp(self):
        self.stack = Stack();
        pass;

    def tearDown(self):
        self.stack = None;
        pass

    def test_push(self):
        # given
        item = 'item1'
        
        # when
        self.stack.push(item)
        
        # then
        self.assertEqual(['item1'], self.stack.get_collection());
